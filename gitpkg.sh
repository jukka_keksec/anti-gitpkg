#!/bin/bash
GITREPO=$1
PKGNAME=$2

WORKDIR=$(/tmp/gitpkg-$RANDOM)
mkdir $WORKDIR
cd $WORKDIR
git clone $GITREPO
if [! -n ./anti-git.sh ]
	#...create the script here
	
# from here we will just make the installer script for 
# pkgtools to use and ensure that the containerize flag
# is set OFF or 0


tar -cfv ./* ~/$PKGNAME.tgz

